package RestAPI.Database.Database.dto;

import lombok.Setter;
import  lombok.AllArgsConstructor;
import  lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Data;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Data
public class EmployeeRequest {
    private long id;
    private String username;
    private String password;
}
