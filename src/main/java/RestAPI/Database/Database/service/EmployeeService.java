package RestAPI.Database.Database.service;

import RestAPI.Database.Database.dto.DeleteRequest;
import RestAPI.Database.Database.dto.EmployeeRequest;
import RestAPI.Database.Database.dto.LoginRequest;
import RestAPI.Database.Database.model.Employee;

import java.util.List;


public interface EmployeeService {
    List<Employee> getAllEmp();
    Employee getEmpbyID(long id);
    Employee addEmp(Employee employee);
    Employee update(EmployeeRequest employeeRequest);
    void delete(DeleteRequest deleteRequest);
    Employee login(LoginRequest loginRequest);
}
