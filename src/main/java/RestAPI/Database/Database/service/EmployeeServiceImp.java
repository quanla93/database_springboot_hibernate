package RestAPI.Database.Database.service;

import RestAPI.Database.Database.dto.DeleteRequest;
import RestAPI.Database.Database.dto.EmployeeRequest;
import RestAPI.Database.Database.dto.LoginRequest;
import RestAPI.Database.Database.exception.ResourceNotFoundException;
import RestAPI.Database.Database.model.Employee;
import RestAPI.Database.Database.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImp implements EmployeeService{
    public final EmployeeRepository employeeRepository;

    public  EmployeeServiceImp(final EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public final List<Employee> getAllEmp() {
        List<Employee> list = employeeRepository.findAll();
        return list;
    }

    @Override
    public final Employee getEmpbyID(long id) {
        Employee e = employeeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Employee"));
        return e;
    }

    @Override
    public final Employee addEmp(Employee employee) {

        return employeeRepository.save(employee);
    }

    @Override
    public final Employee update(EmployeeRequest employeeRequest) {
        Employee e = employeeRepository.findById(employeeRequest.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Not found Employee"));
        e.setUsername(employeeRequest.getUsername());
        e.setPassword(employeeRequest.getPassword());
        employeeRepository.save(e);
        return e;
    }

    @Override
    public final void delete(DeleteRequest deleteRequest) {
        Employee e = employeeRepository.findById(deleteRequest.getId())
                .orElseThrow(() -> new ResourceNotFoundException("Not found Employee"));

        employeeRepository.delete(e);

    }

    @Override
    public Employee login(LoginRequest loginRequest) {
        Employee e = new Employee();
        e.setUsername(loginRequest.getUsername());
        e.setPassword(loginRequest.getPassword());
        return employeeRepository.save(e);
    }

}
