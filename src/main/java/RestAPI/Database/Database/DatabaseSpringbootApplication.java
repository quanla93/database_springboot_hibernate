package RestAPI.Database.Database;

import RestAPI.Database.Database.model.Employee;
import RestAPI.Database.Database.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseSpringbootApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseSpringbootApplication.class, args);
	}

	@Autowired
	private EmployeeRepository employeeRepository;
	@Override
	public void run(String... args) throws Exception {
		Employee e = new Employee();

		e.setUsername("sa");
		e.setPassword("123");
		employeeRepository.save(e);

		Employee e1 = new Employee();
		e1.setUsername("sa1");
		e1.setPassword("1234");
		employeeRepository.save(e1);
	}
}
