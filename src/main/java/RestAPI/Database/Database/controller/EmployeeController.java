package RestAPI.Database.Database.controller;

import RestAPI.Database.Database.dto.DeleteRequest;
import RestAPI.Database.Database.dto.EmployeeRequest;
import RestAPI.Database.Database.dto.LoginRequest;
import RestAPI.Database.Database.model.Employee;

import RestAPI.Database.Database.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/employee")
public class EmployeeController {
       @Autowired
        private final EmployeeService employeeService;

    public EmployeeController(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("")
    public final List<Employee> getAll(){
        return employeeService.getAllEmp();
    }
    @PostMapping("")
    public final Employee addNew(@RequestBody Employee e){
        return employeeService.addEmp(e);
    }
    @GetMapping("{id}")
    public final Employee getEmpbyID(@PathVariable("id") long id){
        return employeeService.getEmpbyID(id);
    }
    @PutMapping("")
    public final ResponseEntity<Employee> update(@RequestBody EmployeeRequest employeeRequest){
        return new ResponseEntity<Employee>(employeeService.update(employeeRequest), HttpStatus.OK);
    }
    @DeleteMapping("")
    public final ResponseEntity<String> delete(@RequestBody DeleteRequest deleteRequest){
        employeeService.delete(new DeleteRequest(deleteRequest.getId()));
        return new ResponseEntity<String>("Delete Success", HttpStatus.OK);
    }
    @PostMapping("/login")
    public final Employee login(@RequestBody LoginRequest loginRequest){
        return  employeeService.login(new LoginRequest(loginRequest.getUsername(),loginRequest.getPassword()));
    }

}
